module GitLab
  module AttributesWithSecrets
    def self.get(node, *path)
      node_attributes = fetch_path(node, path)
      chef_vault = node_attributes['chef_vault']

      if !chef_vault
        # No secrets to mix in
        return node_attributes.to_hash
      end

      chef_vault_item = node_attributes['chef_vault_item'] || node.chef_environment
      secrets = ChefVault::Item.load(chef_vault, chef_vault_item).to_hash
      # The 'id' attribute is used by Chef Vault; don't mix it in later
      secrets.delete('id')

      # Merge secrets into existing configuration data
      Chef::Mixin::DeepMerge.deep_merge(fetch_path(secrets, path), node_attributes)
    end

    # fetch_path(hash, ['foo', 'bar', 'baz'])
    # => hash['foo']['bar']['baz']
    def self.fetch_path(hash, path)
      result = hash
      path.each do |level|
        begin
          result = result[level]
        rescue => ex
          Chef::Log.error "AttributesWithSecrets: Invalid hash path #{path.inspect}"
          raise ex
        end
      end

      result.to_hash
    end
  end
end
