# gitlab-attributes-with-secrets-cookbook

Overwrite node attributes with values from a Chef Vault item at run time.

## Usage

Suppose your recipe's attributes are in `node['my-cookbook']['my-recipe']`. You
want to store some of the attributes in a Chef Vault called 'my-vault' (with
item 'my-item' ) instead of in the node attributes. At the start of your
recipe, write:
    
```
chef_gem 'chef-vault' do
  compile_time true if respond_to?(:compile_time)
end
require 'chef-vault'
my_recipe_conf = GitLab::AttributesWithSecrets.get(node, 'my-cookbook', 'my-recipe')
```

You now have a hash `my_recipe_conf` which you can use wherever you would
otherwise write `node['my-cookbook']['my-recipe']`. At run time, this hash will
contain a mix of regular attributes and secrets.

In the Chef role applied to your nodes that should see the secrets, specify
which vault and vault item to use.

```
{
  "my-cookbook": {
    "my-recipe": {
      "not_secret": "everybody is allowed to see this",
      "chef_vault": "my-vault",
      "chef_vault_item": "my-item"
    }
}
```

In your Chef Vault item, you can just mirror the structure of your node
attributes. Note that the 'id' field is used by Chef Vault itself.

```
{
  "id": "my-item",
  "my-cookbook": {
    "my-recipe": {
      "secret": "need to know only"
    }
  }
}
```

Then at run time, the `my_recipe_conf` hash will look like:

```
{
  "my-cookbook": {
    "my-recipe": {
      "not_secret": "everybody is allowed to see this",
      "secret": "need to know only",
      "chef_vault": "my-vault",
      "chef_vault_item": "my-item"
    }
  }
}
```

## License and Authors

Author:: GitLab B.V. (jacob@gitlab.com)
MIT license
